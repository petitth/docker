# Springboot container

The purpose of this test is to create a container, build a springboot application, and then launch it. <br/>

## The docker file

This script will retrieve a maven container, to build the project. <br/>
On docker run, the application will be launched.
```shell script
FROM maven:3.5-jdk-8

COPY spring-boot-app/src ./src
COPY spring-boot-app/pom.xml ./

# build project inside the container
RUN mvn clean package
# used command on docker run
ENTRYPOINT java -jar target/application.jar
```

## The executable script
This script will launch the build of the container, and launch the application.
```shell script
#!/bin/bash

#
# read me
# -------
#
# This script will create a container with the springboot application, without using nginx.
#
# First make sure the script is executable : chmod +x docker-springboot.sh
# Then, launch it, with : ./docker-springboot.sh
# To test : call this url from a browser : http://localhost:8088/
#
echo "-- cleansing possible existing container --"
docker stop springboot
docker rm springboot --volumes
docker image rm springboot-sandbox:latest

echo "-- creating a new container for the springboot application --"
docker build -t springboot-sandbox -f Dockerfile-springboot .

echo "-- launching the springboot application in the container --"
# container name = 'springboot' - image name = 'springboot-sandbox'
docker run -p 8088:8080 -d --name springboot springboot-sandbox

exit 0
```

## Test it
Open a web browser, and call the application, which is a basic rest service : ```http://localhost:8088```
