# Redis database

<a name="index"></a> 
     
## Table of contents
1. [Introduction](#introduction)                                               
2. [Install and run a Redis server](#install-run-server)               
3. [Using a Redis docker container](#docker-container)                                    
4. [Using Docker Compose to create a Redis container](#docker-compose)                                    
5. [Redis data types](#redis-data-types)                                    
    - [Basic records](#basic-records)  
    - [The lists](#the-lists)  
    - [The sets](#the-sets)  
    - [Hashes](#hashes)  

<a name="introduction"></a>
## Introduction

Redis is in the family of databases called key-value stores.
The essence of a key-value store is the ability to store some data, called a value, inside a key. This data can later be retrieved only if we know the exact key used to store it.
Often Redis it is called a data structure server because it has outer key-value shell, but each value can contain a complex data structure, such as a string, a list, a hashes, or ordered data structures called sorted sets as well as probabilistic data structures like hyperloglog.

source : https://try.redis.io

<a href="#index">back to table of contents</a>

<a name="install-run-server"></a>
## Install and run a Redis server

- download archive, extract and build
```shell script
wget http://download.redis.io/redis-stable.tar.gz
tar xvzf redis-stable.tar.gz
cd redis-stable
# use sudo for the build
make
# test the build
sudo make test
# copy command line interfaces (from src) to /usr/local/bin
sudo make install
```

- start server : ```redis-server``` <br/>
to be able to use client from another machine : ```redis-server --protected-mode no```

- start server with configuration : ```redis-server /etc/redis.conf``` <br/>
You should use the redis.conf file included in the root directory of the Redis source code distribution as a template to write your configuration file. <br/>
See the official examples of redis.conf : https://redis.io/topics/config

- check if Redis is working : ```redis-cli ping``` <br/>

Running redis-cli followed by a command name and its arguments will send this command to the Redis instance running on localhost at port 6379. You can change the host and port used by redis-cli, just try the --help option to check the usage information.

Another interesting way to run redis-cli is without arguments: the program will start in interactive mode, you can type different commands and see their replies.
```shell script
redis-cli
redis 127.0.0.1:6379> ping
PONG
redis 127.0.0.1:6379> set mykey somevalue
OK
redis 127.0.0.1:6379> get foo
"bar"
# get all elements of a list
redis 127.0.0.1:6379> LRANGE color 0 -1
```
- use redis client to access another machine : ```redis-cli -h <host>``` 

- add multiple records at once, from a file
```shell script
cat redis-data | redis-cli --pipe
```

- stop the server
```shell script
redis-cli
127.0.0.1:6379> shutdown
```

- uninstall Redis
```shell script
cd redis-stable
sudo make uninstall
cd /usr/local/bin
# clear all redis files
```

source : https://redis.io/topics/quickstart

<a href="#index">back to table of contents</a>

<a name="docker-container"></a>
## Using a Redis docker container

* basic version : 
```shell script
# get container
docker pull redis
# launch it (detached mode)
docker run --name my-redis -d redis
# check status (-a also show stopped containers)
docker ps -a
# launch it if it's stopped (with stdout/stderr)
docker start -a my-redis 
# stop it if needed
docker stop my-redis
# remove it
docker rm my-redis
```

* create container with custom configuration (launch it from project/docker folder)
```shell script
docker run -v /containers/redis/redis.conf:/usr/local/etc/redis/redis.conf --name my-redis redis redis-server /usr/local/etc/redis/redis.conf
```

* connect to container
```shell script 
# get a bash in the container
docker exec -it my-redis /bin/bash
# launch client
redis-cli
# leave client and terminal (2 times exit command)
exit
```

* get connection infos
```shell script
# server ip
docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' my-redis
# alternate way
docker inspect my-redis | grep "IPAddress"
```

<a href="#index">back to table of contents</a>

<a name="docker-compose"></a>
## Using Docker Compose to create a Redis container 

* create container and add records in database
```
version: '3.8'

services:
  redis:
    image: "redis"
    container_name: my-redis
    command: sh /var/lib/redis/redis-launch.sh
    ports:
      - 6379:6379
    volumes:
      - ./containers/redis/redis-data:/var/lib/redis/data
      - ./containers/redis/redis-launch.sh:/var/lib/redis/redis-launch.sh
      - ./containers/redis/redis.conf:/usr/local/etc/redis/redis.conf
    environment:
      - REDIS_REPLICATION_MODE=master
```

* launching script (redis-launch.sh) 
```shell script
# start server in background and wait for 1 sec
redis-server --daemonize yes && sleep 1
# slurp all data from file to redis in memory db
redis-cli < /var/lib/redis/data
# persist data to disk
redis-cli save
# stop background server
redis-cli shutdown
# start the server normally
redis-server
```

<a href="#index">back to table of contents</a>

<a name="redis-data-types"></a>
## Redis data types

<a name="basic-records"></a>
### Basic records
* add a record with key "server:name" and value "fido"
```
SET server:name "fido"
```

* get a value from a key
```
GET server:name
```

* check if a given key exists or not
```
EXISTS server:name => 1
EXISTS server:blabla => 0
```

* increment a value of a key, or delete a key and associated value
```
SET connections 10
INCR connections => 11
INCR connections => 12
DEL connections
INCR connections => 1
```

<a href="#index">back to table of contents</a>

<a name="the-lists"></a>
### The lists
Redis also supports several more complex data structures. The first one we'll look at is a list. A list is a series of ordered values. Some of the important commands for interacting with lists are RPUSH, LPUSH, LLEN, LRANGE, LPOP, and RPOP. You can immediately begin working with a key as a list, as long as it doesn't already exist as a different type.
This concept is generally true for every Redis data structure: you don't create a key first, and add things to it later, but you can just directly use the command in order to add new elements. As side effect the key will be create if it did not exist. Similarly keys that will result empty after executing some command will automatically be removed from the key space.

RPUSH puts the new element at the end of the list.
```
RPUSH friends "Alice"
RPUSH friends "Bob"
```

LPUSH puts the new element at the start of the list.
```
LPUSH friends "Sam"
```

LRANGE gives a subset of the list. It takes the index of the first element you want to retrieve as its first parameter and the index of the last element you want to retrieve as its second parameter. A value of -1 for the second parameter means to retrieve elements until the end of the list, -2 means to include up to the penultimate, and so forth.
```
LRANGE friends 0 -1 => 1) "Sam", 2) "Alice", 3) "Bob"
LRANGE friends 0 1 => 1) "Sam", 2) "Alice"
LRANGE friends 1 2 => 1) "Alice", 2) "Bob"
```

So far we explored the commands that let you add elements to the list, and LRANGE that let you inspect ranges of the list. A fundamental functionality of Redis lists is the ability to remove, and return to the client at the same time, elements in the head or the tail of the list.

LPOP removes the first element from the list and returns it.
```
LPOP friends => "Sam"
```

RPOP removes the last element from the list and returns it.
```
RPOP friends => "3"
```

Note that the list now only has four element:
```
LLEN friends => 4
LRANGE friends 0 -1 => 1) "Alice" 2) "Bob" 3) "1" 4) "2"
```

Both RPUSH and LPUSH commands are variadic, so you can specify multiple elements in the same command execution.
```
RPUSH friends 1 2 3 => 6
```
Tip: RPUSH and LPUSH return the total length of the list after the operation.

You can also use LLEN to obtain the current length of the list.
```
LLEN friends => 6
```

<a href="#index">back to table of contents</a>

<a name="the-sets"></a>
### The sets

The next data structure that we'll look at is a set. A set is similar to a list, except it does not have a specific order and each element may only appear once. Both the data structures are very useful because while in a list is fast to access the elements near the top or the bottom, and the order of the elements is preserved, in a set is very fast to test for membership, that is, to immediately know if a given element was added or not. Moreover in a set a given element can exist only in a single copy.
Some of the important commands in working with sets are SADD, SREM, SISMEMBER, SMEMBERS and SUNION.

SADD adds the given member to the set, again this command is also variadic.
```
SADD superpowers "flight"
SADD superpowers "x-ray vision" "reflexes"
```

SREM removes the given member from the set, returning 1 or 0 to signal if the member was actually there or not.
```
SREM superpowers "reflexes" => 1
SREM superpowers "making pizza" => 0
```

SISMEMBER tests if the given value is in the set. It returns 1 if the value is there and 0 if it is not.
```
SISMEMBER superpowers "flight" => 1
SISMEMBER superpowers "reflexes" => 0
```

SMEMBERS returns a list of all the members of this set.
```
SMEMBERS superpowers => 1) "flight", 2) "x-ray vision"
```

SUNION combines two or more sets and returns the list of all elements.
```
SADD birdpowers "pecking"
SADD birdpowers "flight"
SUNION superpowers birdpowers => 1) "pecking", 2) "x-ray vision", 3) "flight"
```

The return value of SADD is as important as the one of SREM. If the element we try to add is already inside, then 0 is returned, otherwise SADD returns 1:
```
SADD superpowers "flight" => 0
SADD superpowers "invisibility" => 1
```

Sets also have a command very similar to LPOP and RPOP in order to extract elements from the set and return them to the client in a single operation. However since sets are not ordered data structures the returned (and removed) elements are totally casual in this case.
```
SADD letters a b c d e f => 6
SPOP letters 2 => 1) "c" 2) "a"
```

The argument of SPOP after the name of the key, is the number of elements we want it to return, and remove from the set.
Now the set will have just the remaining elements:
```
SMEMBERS letters => 1) "b" 2) "d" 3) "e" 4) "f"
```
There is also a command to return random elements without removing such elements from the set, it is called SRANDMEMBER. You can try it yourself, the arguments are similar to SPOP, but if you specify a negative count instead of a positive one, it may also return repeating elements.
Sets are a very handy data type, but as they are unsorted they don't work well for a number of problems. This is why Redis 1.2 introduced Sorted Sets.
A sorted set is similar to a regular set, but now each value has an associated score. This score is used to sort the elements in the set.
```
ZADD hackers 1940 "Alan Kay"
ZADD hackers 1906 "Grace Hopper"
ZADD hackers 1953 "Richard Stallman"
ZADD hackers 1965 "Yukihiro Matsumoto"
ZADD hackers 1916 "Claude Shannon"
ZADD hackers 1969 "Linus Torvalds"
ZADD hackers 1957 "Sophie Wilson"
ZADD hackers 1912 "Alan Turing"
```

In these examples, the scores are years of birth and the values are the names of famous hackers.
```
ZRANGE hackers 2 4 => 1) "Claude Shannon", 2) "Alan Kay", 3) "Richard Stallman"
```

<a href="#index">back to table of contents</a>

<a name="hashes"></a>
### Hashes

Simple strings, sets and sorted sets already get a lot done but there is one more data type Redis can handle: Hashes.
Hashes are maps between string fields and string values, so they are the perfect data type to represent objects (eg: A User with a number of fields like name, surname, age, and so forth):
```
HSET user:1000 name "John Smith"
HSET user:1000 email "john.smith@example.com"
HSET user:1000 password "s3cret"
```

To get back the saved data use HGETALL:
```
HGETALL user:1000
```

You can also set multiple fields at once:
```
HMSET user:1001 name "Mary Jones" password "hidden" email "mjones@example.com"
```

If you only need a single field value that is possible as well:
```
HGET user:1001 name => "Mary Jones"
```

Numerical values in hash fields are handled exactly the same as in simple strings and there are operations to increment this value in an atomic way.
```
HSET user:1000 visits 10
HINCRBY user:1000 visits 1 => 11
HINCRBY user:1000 visits 10 => 21
HDEL user:1000 visits
HINCRBY user:1000 visits 1 => 1
```

Check the full list of Hash commands for more information : https://redis.io/commands#hash


source : https://try.redis.io

<a href="#index">back to table of contents</a>