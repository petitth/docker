# Postgres database

<a name="index"></a> 
     
## Table of contents
1. [Introduction](#introduction)  
2. [Using a Postgres docker container](#docker-container)
3. [Using Docker Compose to create a Postgres container](#docker-compose)                                    
4. [Connect to database with a client](#postgres-client)
    - [Psql](#client-psql)  
    - [Pgcli](#client-pgcli)  
    - [DBeaver](#client-dbeaver)  

<a name="introduction"></a>
## Introduction

PostgreSQL is a powerful, open source object-relational database system with over 30 years of active development that has earned it a strong reputation for reliability, feature robustness, and performance. 

source : https://www.postgresql.org/

<a href="#index">back to table of contents</a>

<a name="docker-container"></a>
## Using a Postgres docker container

Create a container with no database :
```
docker run --name my-postgres -e POSTGRES_PASSWORD=somepassword -d postgres
```

source : https://registry.hub.docker.com/_/postgres

<a href="#index">back to table of contents</a>

<a name="docker-compose"></a>
## Using Docker Compose to create a Postgres container

This is an example of docker-compose.yaml where all sql scripts in postgresql folder will be executed. <br/>
Launch it with ```docker-compose up``` or ```docker-compose up postgres``` if there are other containers defined.
```
version: '3.8'

services:
  postgres:
    image: "postgres"
    container_name: my-postgres
    ports:
      - 5432:5432
    volumes:
      - ./containers/postgresql/:/docker-entrypoint-initdb.d
    environment:
      - POSTGRES_USER=postgres
      - POSTGRES_PASSWORD=somepassword
      - POSTGRES_DB=postgres
    networks:
      static-network:
        ipv4_address: 172.20.0.3
networks:
  static-network:
    ipam:
      config:
        - subnet: 172.20.0.0/16
```

<a href="#index">back to table of contents</a>

<a name="postgres-client"></a>
## Connect to database with a client

<a name="client-psql"></a>
### Psql

Psql is the standard command line client, provided with server installation. <br/>
Launch it from docker host : 
```shell script
docker exec -it my-postgres /bin/bash
psql --host=my-postgres --username=postgres --dbname=postgres
# create database from file
\i /tmp/data
```

some commands :
- connect to a database : ```psql -d database -U  user -W```
- connect to a database on different host : ```psql -h host -d database -U user -W```
- list available databases : ```\l```
- list available tables : ```\dt```
- list available schemas : ```\dn```
- list available views : ```\dv```
- list users and their roles : ```\du```
- execute commands from a file : ```\i filename```
- available commands : ```\?```

official : https://www.postgresql.org/docs/current/app-psql.html <br/>
other link : https://www.postgresqltutorial.com/psql-commands

<a name="client-pgcli"></a>
### Pgcli

Pgcli is a command line client. <br/>
Install it with python packet : 
```shell script
sudo apt-get install libpq-dev python-dev
sudo pip install pgcli
``` 
See usage : ```pgcli --help``` <br/>
Examples :

- connect from docker host machine : 
```shell script
psql --host=my-postgres --username=postgres --dbname=postgres
```
then launch query

source : https://www.pgcli.com/

<a href="#index">back to table of contents</a>

<a name="client-dbeaver"></a>
### DBeaver

DBeaver is a free universal database tool with a GUI. <br/>
Use ip address of docker host machine when connecting. 

source : https://dbeaver.io

<a href="#index">back to table of contents</a>
