# Oracle database

<a name="container-image"></a>
## Create container image

* clone oracle docker images : https://github.com/oracle/docker-images.git

* get binaries of db : download zip, manually, from   
  https://www.oracle.com/database/technologies/oracle-database-software-downloads.html

* copies binaries
```bash
cd docker-images/OracleDatabase/SingleInstance/dockerfiles
cd 19.3.0
cp ~/Download/LINUX.X64_193000_db_home.zip .
```

* create image
```bash
cd ..
./buildContainerImage.sh -v 19.3.0 -e
```

source : https://dzone.com/articles/create-an-oracle-database-docker-image


<a name="docker-compose"></a>
## Using Docker Compose to create an Oracle container

```yaml
version: '3.8'
#
# Launch oracle database docker container.
# - to launch : docker-compose -f docker-compose-db-oracle.yaml up
#
networks:
  static-network:
    name: db_network
    ipam:
      config:
        - subnet: 172.22.0.0/16

services:
  #
  # oracle container : compatible x86-64, Linux (no ARM)
  # ----------------
  # * prerequisites :
  #   - make sure all sh scripts in /containers/oracle are executable with chmod +x on each
  #   - follow procedure to create container image before
  #
  # * to connect : user = oracle19, pwd = oracle19, host = 172.22.0.10,
  #                service name = ORCLPDB1, port = 1521
  oracle19:
    image: "oracle/database:19.3.0-ee"
    container_name: my-oracle19
    ports:
      - 1523:1521
      - 5500:5500
    volumes:
      - ./containers/oracle/sql:/docker-entrypoint-initdb.d/setup
      - ./containers/oracle/scripts:/docker-entrypoint-initdb.d/startup
    environment:
      - INIT_SGA_SIZE=1024
      - INIT_PGA_SIZE=1024
      - ORACLE_CHARACTERSET=AL32UTF8
      - NLS_LANG=AMERICAN_AMERICA.AL32UTF8
    networks:
      static-network:
        ipv4_address: 172.22.0.10
```
