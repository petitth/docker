# Ubuntu/Debian containers

## Introduction

The purpose of this note is to learn how to create an Ubuntu or Debian container. <br/>

## Quick way

This simple way will launch the container and connect to a bash as root. <br/>
If you exit the container, it will be stopped. <br/>
remark : 'ubuntu-bash' is the container name, it can be edited to any value you want

```shell script
docker run -it --name ubuntu-bash ubuntu /bin/bash
docker run -it --name debian-bash debian /bin/bash
```

## Create a container which keeps running using commands

This version of the run command will launch a never ending process. <br/>
Therefore, the container will stay up.

```shell script
# -d flag means detach

# ubuntu version
docker run -d --name ubuntu-bash ubuntu tail -f /dev/null
docker exec -it ubuntu-bash /bin/bash
# debian version
docker run -d --name debian-bash debian tail -f /dev/null
docker exec -it debian-bash /bin/bash
```

## Create a container which keeps running using Dockerfile

Use the launch script, then connect to container with : ```docker exec -it ubuntu-bash /bin/bash```

* Dockerfile : second line installs curl tool (not mandatory)
```shell script
FROM ubuntu:latest
RUN  apt-get update && apt-get install -y curl
CMD tail -f /dev/null
```

* env_vars (not mandatory)
```shell script
FOO=bar
```

* launch script
```shell script
#!/bin/bash
#
# Create an ubuntu container which keeps running.
#

# constants
container_name="ubuntu-bash"
container_image="custom-ubuntu"

# use ansi escape codes for colors
NOCOLOR='\033[0m'
BLUE='\033[0;34m'
CYAN='\033[0;36m'

echo -e "${CYAN}-- cleansing possible existing container --${NOCOLOR}"
docker stop ${container_name}
docker rm ${container_name} --volumes
docker image rm ${container_image}:latest

echo -e "${CYAN}-- creating a new container '${container_name}' --${NOCOLOR}"
docker build -t ${container_image} .

echo -e "${CYAN}-- launching the container --${NOCOLOR}"
docker run -d --env-file ./env_vars --name ${container_name} ${container_image}

exit 0
```
