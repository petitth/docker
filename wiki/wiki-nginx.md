# NGINX containers

## Create a simple web server

- create a very basic index.html and store it into ```/containers/nginx-web```

```
version: "3.8"

services:
  nginx:
    image: nginx:latest
    hostname: nginx-basic
    ports:
      - "8003:80"
    volumes:
      - ./containers/nginx-web/www:/usr/share/nginx/html

```

- start the container : ```docker-compose -f nginx-docker-compose up```

- test it with a web browser, using ```localhost:8003```
- you can also use curl, and retrieve html code of the index : ```curl localhost:8003```

## Create a load balancer

For this test, 3 nginx containers will be created : 2 as web servers, and the other one as load balancer. 

```
version: "3.8"

services:
  nginx-web1:
    image: nginx:latest
    container_name: nginx-web1
    ports:
      - "8003:80"
    volumes:
      - ./containers/nginx-web/www:/usr/share/nginx/html
  nginx-web2:
    image: nginx:latest
    container_name: nginx-web2
    ports:
      - "8004:80"
    volumes:
      - ./containers/nginx-web2/www:/usr/share/nginx/html
  nginx-load-balancer:
    image: nginx:latest
    container_name: load-balancer
    ports:
      - "8005:80"
    volumes:
     - ./containers/load-balancer/nginx.conf:/etc/nginx/nginx.conf

```

The config of the load balancer comes from default version of ```nginx.conf```, replacing ```http``` block.
The setup is defined with host names, but it would also work with ip addresses. The problem with ip addresses is that Docker may allocate different ones, unless you force static ones. 
```
http {
        upstream backend {
                server nginx-web1:80;
                server nginx-web2:80;
        }

        server {
                listen 80;
                location / {
                        proxy_pass http://backend;
                }
        }
}
```

Now you can validate : open your web browser and call ```localhost:8005```. <br/>
Refreshing the page will switch from one server to the other. <br/><br/>
By default, the Nginx load balancer uses a round-robin setup, wherein workloads are equally passed among the nodes, one after the next. There are other load-balancing options, including IP hash-based routing, which is used for sticky sessions -- or instances where the same server handles all requests for a particular user.

source : <br/> 
https://searchitoperations.techtarget.com/tutorial/Control-Docker-containers-with-the-Nginx-load-balancer

## Create a web server running a spring boot application

This test will use a spring boot application with minimal code, exposing a rest service. <br/>
It will be deployed on nginx container, and launched with maven.

- docker compose file
```
version: "3.8"

services:
  nginx-spring-boot:
    container_name: nginx-spring-boot
    image: nginx:latest
    restart: always
    ports:
      - "8006:80"
      - 443:443
    volumes:
      - ./containers/nginx-spring-boot/conf.d:/etc/nginx/conf.d
    depends_on:
      - spring-boot-app

  spring-boot-app:
    restart: always
    build: ./containers/nginx-spring-boot
    working_dir: /spring-boot-app
    volumes:
      - ./spring-boot-app:/spring-boot-app
    expose:
      - "8080"
    command: mvn clean spring-boot:run
```

- conf.d/app.conf
```
server {
    listen 80;
    charset utf-8;
    access_log off;

    location / {
        proxy_pass http://spring-boot-app:8080;
        proxy_set_header Host $host:$server_port;
        proxy_set_header X-Forwarded-Host $server_name;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    }

    location /static {
        access_log   off;
        expires      30d;

        alias /spring-boot-app/static;
    }
}
```

- don't forget to add ```Dockerfile``` in containers/nginx-spring-boot, for the build :
```
FROM maven:3.5-jdk-8
```

- start the container : ```docker-compose -f nginx-springboot-docker-compose up```

- test the spring boot application, with your browser : ```http://localhost:8006```

source : https://hellokoding.com/spring-boot/docker/