# ElasticSearch

## Queries examples

* check that nodes are up and running

```curl -u elastic:elasticpwd -X GET "http://localhost:9200/_cat/nodes?v=true&pretty"```

* create an index called suppliers

```curl -u elastic:elasticpwd -X PUT "http://localhost:9200/supplier?pretty"```

* count records

```curl -u elastic:elasticpwd -X POST "http://localhost:9200/supplier/_count"```

* check index content

```curl -u elastic:elasticpwd -X GET "http://localhost:9200/supplier?pretty"```

* get a record

```curl -u elastic:elasticpwd -X GET "Content-Type: application/json" 'http://localhost:9200/supplier/_doc/1'```