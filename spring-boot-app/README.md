# Testing a web application with Spring Boot

This is a Spring Boot web application with minimal code, used to test deployment on Docker.

- to launch project : **mvn spring-boot:run**   
- alternate launch : **mvn package && java -jar target/application.jar**   
- to test it : open your web browser and call : **http://localhost:8080/**   
remark : if default port 8080 is already used on your machine, feel free to redefine ```server.port``` property
