# Docker

<a name="index"></a>

## Table of contents
1. [Introduction](#introduction)
2. [Docker Commands](#docker-commands)
3. [Installation](#installation)
   - [Install Docker on a Debian and Ubuntu](#install-docker-debian)
   - [Install Docker on a Raspberry Pi](#install-docker-raspberry)
   - [Install Docker Compose on Debian and Ubuntu](#install-docker-compose-debian)
   - [Install Docker Compose on a Raspberry Pi](#install-docker-compose-raspberry)
4. [Get new containers](#get-containers)
5. [Misc](#misc)
   - [Move your docker folder](#move-docker-folder)
   - [Error "x509: certificate signed by unknown authority"](#error-certificate-unknown-authority)

<a name="introduction"></a>
## Introduction

Docker is a set of platform as a service (PaaS) products that uses OS-level virtualization to deliver software in packages called containers.  
Containers are isolated from one another and bundle their own software, libraries and configuration files; they can communicate with each other through well-defined channels.
All containers are run by a single operating system kernel and therefore use fewer resources than virtual machines.  
link : www.docker.com

<a href="#index">back to index</a>

<a name="docker-commands"></a>
## Docker commands

### Basic commands
- check version : ```docker --version```
- check that docker is running, launching dummy container :  ```docker run hello-world```
- same giving a local name to container :  ```docker run --name hello-container hello-world```
- connect to a named container : ```docker exec -it <container-name> /bin/bash```
- retrieve a file from container : ``` docker cp <container-name>:/tmp/something.jar /tmp```
- check containers ip address : ```docker inspect <container-name> | grep "IPAddress"```
- start Docker : ```systemctl start docker```
- stop Docker : ```systemctl stop docker```
---

- list all docker processes : ```docker ps -a```
- remove a container and associated volumes : ```docker rm <container-name> --volumes```
- stop a container : ```docker stop <container-name>```
- start a container : ```docker start <container-name>```
- restart a container : ```docker restart <container-name>```

### Logs
- check container log : ```docker logs <container-name>```
- get last 50 lines of log : ```docker logs -f --tail 50 <container-name>```
- get log events after a certain time : ```docker logs --since 2020-01-01T10:00:00 <container-name>```

### Images / Volumes / Network
- existing images : ```docker image ls```
- remove an image using id : ```docker image rm <image-id>```
- force removing an image using id : ```docker image rm <image-id> --force```
- remove an image using repository : ```docker image rm sandbox:latest```
- check created networks : ```docker network ls```
- remove one network : ```docker network rm <name>```
- check existing volumes : ```docker volume ls```
- remove one volume : ```docker volume rm <volume-id>```
- clear all non used volumes : ```docker volume prune --force```

### Docker-compose
- status with docker-compose : ```docker-compose ps```
- build/start containers with docker compose :
```bash
docker-compose -f <yaml-file> up
# detach mode
docker-compose -f <yaml-file> up -d
```

### Docker run
- docker run forcing ip
```bash
# create network
docker network create --subnet=172.18.0.0/16 mynet123

# launch new container, added to existing network
docker run --network mynet123 --ip 172.18.0.22 -it ubuntu /bin/bash
```

<a href="#index">back to index</a>

<a name="installation"></a>
## Installation

<a name="install-docker-debian"></a>
### Install Docker on Debian and Ubuntu

- get deb archive and dependencies : go to ```https://download.docker.com/linux/debian/dists/```,
  then ```pool/stable/```  
  (adapt url for Ubuntu)
- after installing the archive, add user to docker group
```bash
su - # use sudo if ubuntu 
usermod -aG docker <user>
```
- restart

- check the installation with hello-world container :
  ```docker run hello-world```

source : https://docs.docker.com/engine/install/debian/

<a href="#index">back to index</a>


<a name="install-docker-raspberry"></a>
### Install Docker on a Raspberry Pi

- get install script
```bash
curl -fsSL https://get.docker.com -o get-docker.sh
```

- launch it
```bash
sudo sh get-docker.sh
```

- add right to default pi user to be able to use it
```bash
sudo usermod -aG docker pi
```

- restart your session to get the new right on pi user

- test that installation worked with hello world container
```bash
docker run hello-world
```

source :
https://www.framboise314.fr/installer-docker-sur-raspberry-pi

<a href="#index">back to index</a>

<a name="install-docker-compose-debian"></a>
### Install Docker Compose on Debian and Ubuntu

- launch it :
```bash
# download file from https://github.com/docker/compose/releases 
# make it executable
chmod +x docker-compose-linux-x86_64
# create a shortcut to system folder
ln -s /home/USER/docker-compose-linux-x86_64 /usr/local/bin/docker-compose
```

- check installation : ```docker-compose --version```

source : https://docs.docker.com/compose/install

<a href="#index">back to index</a>

<a name="install-docker-compose-raspberry"></a>
### Install Docker Compose on a Raspberry Pi

- prerequisites
```bash
sudo apt-get install python3-distutils python3-dev libffi-dev libssl-dev
```

- get Python packet manager
```bash
curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
sudo python3 get-pip.py
```

- install Docker Compose
```bash
sudo pip3 install docker-compose
```

- check that installation worked
```bash
docker-compose --version
```

source :
https://www.framboise314.fr/installer-docker-sur-raspberry-pi

<a href="#index">back to index</a>

<a name="get-containers"></a>
## Get new containers

The following link contains a filter to get raspberry pi compatible containers :   
https://registry.hub.docker.com/search?q=&type=image&architecture=arm%2Carm64

<a href="#index">back to index</a>

<a name="misc"></a>
## Misc

<a name="move-docker-folder"></a>
### Move your docker folder

Docker is facing errors because you main partition is getting low on disk space.   
The following procedure has been tested on Debian and Raspbian successfully.

1) stop the docker daemon : ```sudo systemctl stop docker```

2) add a file named **daemon.json** under the directory **/etc/docker** with the following content :
```json
{
   "data-root": "/path/to/your/docker"
}
```
remark : previous name attribute ```graph``` is now deprecated

3) copy the current data directory to the new one : ```sudo rsync -aP /var/lib/docker/ /path/to/your/docker```

remark : if rsync is not available, just install it with ```apt-get install rsync```

4) rename the old docker directory : ```sudo mv /var/lib/docker /var/lib/docker.old```

5) restart the docker daemon : ```sudo systemctl start docker```

6) test that docker is still working

7) clean the old folder : ```sudo rm -rf /var/lib/docker.old```

source : https://www.guguweb.com/2019/02/07/how-to-move-docker-data-directory-to-another-location-on-ubuntu/

<a href="#index">back to index</a>

<a name="error-certificate-unknown-authority"></a>
### Error "x509: certificate signed by unknown authority"

As root, add an entry to ```/etc/docker/daemon.json``` :   
```{ "insecure-registries" : ["server.hostname.com:5443"] }```

source : https://www.digitalsanctuary.com/general/solution-for-docker-registry-error-certificate-signed-by-unknown-authority.html

<a href="#index">back to index</a>
