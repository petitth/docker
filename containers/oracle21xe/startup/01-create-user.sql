ALTER SESSION SET CONTAINER = XEPDB1;

-- create user
create user oracle21xe identified by oracle21xe;
-- add permissions
GRANT ALL PRIVILEGES TO oracle21xe;
