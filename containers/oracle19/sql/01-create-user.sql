-- connect to ORACLE_SID
ALTER SESSION SET CONTAINER = ORCLPDB1;

-- create user
create user oracle19 identified by oracle19;
-- add permissions
GRANT ALL PRIVILEGES TO oracle19;
