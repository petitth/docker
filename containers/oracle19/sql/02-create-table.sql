-- connect to ORACLE_SID
ALTER SESSION SET CONTAINER = ORCLPDB1;

ALTER SESSION SET CURRENT_SCHEMA = oracle19;

CREATE TABLE "T_USERS"
(
    "USER_ID" NUMBER NOT NULL,
    "FIRST_NAME" VARCHAR2(30 CHAR) NOT NULL,
    "LAST_NAME" VARCHAR2(30 CHAR) NOT NULL,
    "ADDRESS" VARCHAR2(60 CHAR) NULL,
    "CITY" VARCHAR2(30 CHAR) NULL,
    "POSTAL_CODE" VARCHAR2(10 CHAR),
    "COUNTRY" VARCHAR2(15 CHAR) NOT NULL,
    "PHONE" VARCHAR2(24 CHAR) NULL,
    PRIMARY KEY(USER_ID)
) ;


insert into "T_USERS" (USER_ID, FIRST_NAME, LAST_NAME, ADDRESS, CITY, POSTAL_CODE, COUNTRY, PHONE) VALUES (1,'Maria','Anders','Obere Str. 57','Berlin','12209','Germany','030-0074321');
insert into "T_USERS" (USER_ID, FIRST_NAME, LAST_NAME, ADDRESS, CITY, POSTAL_CODE, COUNTRY, PHONE) VALUES (2,'Ana','Trujillo','Avda. de la Constituciùn 2222','México D.F.','05021','Mexico','(5) 555-4729');
insert into "T_USERS" (USER_ID, FIRST_NAME, LAST_NAME, ADDRESS, CITY, POSTAL_CODE, COUNTRY, PHONE) VALUES (3,'Antonio','Moreno','Mataderos  2312','México D.F.','05023','Mexico','(5) 555-3932');
insert into "T_USERS" (USER_ID, FIRST_NAME, LAST_NAME, ADDRESS, CITY, POSTAL_CODE, COUNTRY, PHONE) VALUES (4,'Thomas','Hardy','120 Hanover Sq.','London','WA1 1DP','UK','(171) 555-7788');
insert into "T_USERS" (USER_ID, FIRST_NAME, LAST_NAME, ADDRESS, CITY, POSTAL_CODE, COUNTRY, PHONE) VALUES (5,'Christina','Berglund','Berguvsvègen  8','Luleî','S-958 22','Sweden','0921-12 34 65');
insert into "T_USERS" (USER_ID, FIRST_NAME, LAST_NAME, ADDRESS, CITY, POSTAL_CODE, COUNTRY, PHONE) VALUES (6,'Hanna','Moos','Forsterstr. 57','Mannheim','68306','Germany','0621-08460');
insert into "T_USERS" (USER_ID, FIRST_NAME, LAST_NAME, ADDRESS, CITY, POSTAL_CODE, COUNTRY, PHONE) VALUES (7,'Frédérique','Citeaux','24, place Kléber','Strasbourg','67000','France','88.60.15.31');
insert into "T_USERS" (USER_ID, FIRST_NAME, LAST_NAME, ADDRESS, CITY, POSTAL_CODE, COUNTRY, PHONE) VALUES (8,'MartÆn','Sommer','C/ Araquil, 67','Madrid','28023','Spain','(91) 555 22 82');
insert into "T_USERS" (USER_ID, FIRST_NAME, LAST_NAME, ADDRESS, CITY, POSTAL_CODE, COUNTRY, PHONE) VALUES (9,'Laurence','Lebihan','12, rue des Bouchers','Marseille','13008','France','91.24.45.40');
insert into "T_USERS" (USER_ID, FIRST_NAME, LAST_NAME, ADDRESS, CITY, POSTAL_CODE, COUNTRY, PHONE) VALUES (10,'Elizabeth','Lincoln','23 Tsawassen Blvd.','Tsawassen','T2F 8M4','Canada','(604) 555-4729');
insert into "T_USERS" (USER_ID, FIRST_NAME, LAST_NAME, ADDRESS, CITY, POSTAL_CODE, COUNTRY, PHONE) VALUES (11,'Victoria','Ashworth','Fauntleroy Circus','London','EC2 5NT','UK','(171) 555-1212');
insert into "T_USERS" (USER_ID, FIRST_NAME, LAST_NAME, ADDRESS, CITY, POSTAL_CODE, COUNTRY, PHONE) VALUES (12,'Patricio','Simpson','Cerrito 333','Buenos Aires','1010','Argentina','(1) 135-5555');
insert into "T_USERS" (USER_ID, FIRST_NAME, LAST_NAME, ADDRESS, CITY, POSTAL_CODE, COUNTRY, PHONE) VALUES (13,'Francisco','Chang','Sierras de Granada 9993','México D.F.','05022','Mexico','(5) 555-3392');
insert into "T_USERS" (USER_ID, FIRST_NAME, LAST_NAME, ADDRESS, CITY, POSTAL_CODE, COUNTRY, PHONE) VALUES (14,'Yang','Wang','Hauptstr. 29','Bern','3012','Switzerland','0452-076545');
insert into "T_USERS" (USER_ID, FIRST_NAME, LAST_NAME, ADDRESS, CITY, POSTAL_CODE, COUNTRY, PHONE) VALUES (15,'Pedro','Afonso','Av. dos LusÆadas, 23','Sao Paulo','05432-043','Brazil','(11) 555-7647');
insert into "T_USERS" (USER_ID, FIRST_NAME, LAST_NAME, ADDRESS, CITY, POSTAL_CODE, COUNTRY, PHONE) VALUES (16,'Elizabeth','Brown','Berkeley Gardens 12  Brewery','London','WX1 6LT','UK','(171) 555-2282');
insert into "T_USERS" (USER_ID, FIRST_NAME, LAST_NAME, ADDRESS, CITY, POSTAL_CODE, COUNTRY, PHONE) VALUES (17,'Sven','Ottlieb','Walserweg 21','Aachen','52066','Germany','0241-039123');
insert into "T_USERS" (USER_ID, FIRST_NAME, LAST_NAME, ADDRESS, CITY, POSTAL_CODE, COUNTRY, PHONE) VALUES (18,'Janine','Labrune','67, rue des Cinquante Otages','Nantes','44000','France','40.67.88.88');
insert into "T_USERS" (USER_ID, FIRST_NAME, LAST_NAME, ADDRESS, CITY, POSTAL_CODE, COUNTRY, PHONE) VALUES (19,'Ann','Devon','35 King George','London','WX3 6FW','UK','(171) 555-0297');
insert into "T_USERS" (USER_ID, FIRST_NAME, LAST_NAME, ADDRESS, CITY, POSTAL_CODE, COUNTRY, PHONE) VALUES (20,'Roland','Mendel','Kirchgasse 6','Graz','8010','Austria','7675-3425');
insert into "T_USERS" (USER_ID, FIRST_NAME, LAST_NAME, ADDRESS, CITY, POSTAL_CODE, COUNTRY, PHONE) VALUES (21,'Aria','Cruz','Rua Orùs, 92','Sao Paulo','05442-030','Brazil','(11) 555-9857');
insert into "T_USERS" (USER_ID, FIRST_NAME, LAST_NAME, ADDRESS, CITY, POSTAL_CODE, COUNTRY, PHONE) VALUES (22,'Die','Roel','C/ Moralzarzal, 86','Madrid','28034','Spain','(91) 555 94 44');
insert into "T_USERS" (USER_ID, FIRST_NAME, LAST_NAME, ADDRESS, CITY, POSTAL_CODE, COUNTRY, PHONE) VALUES (23,'Martine','Rancé','184, chaussée de Tournai','Lille','59000','France','20.16.10.16');
insert into "T_USERS" (USER_ID, FIRST_NAME, LAST_NAME, ADDRESS, CITY, POSTAL_CODE, COUNTRY, PHONE) VALUES (24,'Maria','Larsson','ökergatan 24','Brècke','S-844 67','Sweden','0695-34 67 21');
insert into "T_USERS" (USER_ID, FIRST_NAME, LAST_NAME, ADDRESS, CITY, POSTAL_CODE, COUNTRY, PHONE) VALUES (25,'Peter','Franken','Berliner Platz 43','München','80805','Germany','089-0877310');
insert into "T_USERS" (USER_ID, FIRST_NAME, LAST_NAME, ADDRESS, CITY, POSTAL_CODE, COUNTRY, PHONE) VALUES (26,'Carine','Schmitt','54, rue Royale','Nantes','44000','France','40.32.21.21');
insert into "T_USERS" (USER_ID, FIRST_NAME, LAST_NAME, ADDRESS, CITY, POSTAL_CODE, COUNTRY, PHONE) VALUES (27,'Paolo','Accorti','Via Monte Bianco 34','Torino','10100','Italy','011-4988260');
insert into "T_USERS" (USER_ID, FIRST_NAME, LAST_NAME, ADDRESS, CITY, POSTAL_CODE, COUNTRY, PHONE) VALUES (28,'Lino','Rodriguez','Jardim das rosas n. 32','Lisboa','1675','Portugal','(1) 354-2534');
insert into "T_USERS" (USER_ID, FIRST_NAME, LAST_NAME, ADDRESS, CITY, POSTAL_CODE, COUNTRY, PHONE) VALUES (29,'Eduardo','Saavedra','Rambla de Cataluûa, 23','Barcelona','08022','Spain','(93) 203 4560');
insert into "T_USERS" (USER_ID, FIRST_NAME, LAST_NAME, ADDRESS, CITY, POSTAL_CODE, COUNTRY, PHONE) VALUES (30,'José','Pedro Freyre','C/ Romero, 33','Sevilla','41101','Spain','(95) 555 82 82');
insert into "T_USERS" (USER_ID, FIRST_NAME, LAST_NAME, ADDRESS, CITY, POSTAL_CODE, COUNTRY, PHONE) VALUES (31,'André','Fonseca','Av. Brasil, 442','Campinas','04876-786','Brazil','(11) 555-9482');
insert into "T_USERS" (USER_ID, FIRST_NAME, LAST_NAME, ADDRESS, CITY, POSTAL_CODE, COUNTRY, PHONE) VALUES (32,'Howard','Snyder','2732 Baker Blvd.','Eugene','97403','USA','(503) 555-7555');
insert into "T_USERS" (USER_ID, FIRST_NAME, LAST_NAME, ADDRESS, CITY, POSTAL_CODE, COUNTRY, PHONE) VALUES (33,'Manuel','Pereira','5 Ave. Los Palos Grandes','Caracas','1081','Venezuela','(2) 283-2951');
insert into "T_USERS" (USER_ID, FIRST_NAME, LAST_NAME, ADDRESS, CITY, POSTAL_CODE, COUNTRY, PHONE) VALUES (34,'Mario','Pontes','Rua do Paìo, 67','Rio de Janeiro','05454-876','Brazil','(21) 555-0091');
insert into "T_USERS" (USER_ID, FIRST_NAME, LAST_NAME, ADDRESS, CITY, POSTAL_CODE, COUNTRY, PHONE) VALUES (35,'Carlos','Hernandez','Carrera 22 con Ave. Carlos Soublette #8-35','San Cristùbal','5022','Venezuela','(5) 555-1340');
insert into "T_USERS" (USER_ID, FIRST_NAME, LAST_NAME, ADDRESS, CITY, POSTAL_CODE, COUNTRY, PHONE) VALUES (36,'Yoshi','Latimer','City Center Plaza 516 Main St.','Elgin','97827','USA','(503) 555-6874');
insert into "T_USERS" (USER_ID, FIRST_NAME, LAST_NAME, ADDRESS, CITY, POSTAL_CODE, COUNTRY, PHONE) VALUES (37,'Patricia','McKenna','8 Johnstown Road','Cork','Co. Cork','Ireland','2967 542');
insert into "T_USERS" (USER_ID, FIRST_NAME, LAST_NAME, ADDRESS, CITY, POSTAL_CODE, COUNTRY, PHONE) VALUES (38,'Helen','Bennett','Garden House Crowther Way','Cowes','PO31 7PJ','UK','(198) 555-8888');
insert into "T_USERS" (USER_ID, FIRST_NAME, LAST_NAME, ADDRESS, CITY, POSTAL_CODE, COUNTRY, PHONE) VALUES (39,'Philip','Cramer','Maubelstr. 90','Brandenburg','14776','Germany','0555-09876');
insert into "T_USERS" (USER_ID, FIRST_NAME, LAST_NAME, ADDRESS, CITY, POSTAL_CODE, COUNTRY, PHONE) VALUES (40,'Daniel','Tonini','67, avenue de l''Europe','Versailles','78000','France','30.59.84.10');
insert into "T_USERS" (USER_ID, FIRST_NAME, LAST_NAME, ADDRESS, CITY, POSTAL_CODE, COUNTRY, PHONE) VALUES (41,'Annette','Roulet','1 rue Alsace-Lorraine','Toulouse','31000','France','61.77.61.10');
insert into "T_USERS" (USER_ID, FIRST_NAME, LAST_NAME, ADDRESS, CITY, POSTAL_CODE, COUNTRY, PHONE) VALUES (42,'Yoshi','Tannamuri','1900 Oak St.','Vancouver','V3F 2K1','Canada','(604) 555-3392');
insert into "T_USERS" (USER_ID, FIRST_NAME, LAST_NAME, ADDRESS, CITY, POSTAL_CODE, COUNTRY, PHONE) VALUES (43,'John','Steel','12 Orchestra Terrace','Walla Walla','99362','USA','(509) 555-7969');
insert into "T_USERS" (USER_ID, FIRST_NAME, LAST_NAME, ADDRESS, CITY, POSTAL_CODE, COUNTRY, PHONE) VALUES (44,'Renate','Messner','Magazinweg 7','Frankfurt','60528','Germany','069-0245984');
insert into "T_USERS" (USER_ID, FIRST_NAME, LAST_NAME, ADDRESS, CITY, POSTAL_CODE, COUNTRY, PHONE) VALUES (45,'Jaime','Yorres','87 Polk St. Suite 5','San Francisco','94117','USA','(415) 555-5938');
insert into "T_USERS" (USER_ID, FIRST_NAME, LAST_NAME, ADDRESS, CITY, POSTAL_CODE, COUNTRY, PHONE) VALUES (46,'Carlos','Sanchez','Carrera 52 con Ave. BolÆvar #65-98 Llano Lar','Barquisimeto','3508','Venezuela','(9) 331-6954');
insert into "T_USERS" (USER_ID, FIRST_NAME, LAST_NAME, ADDRESS, CITY, POSTAL_CODE, COUNTRY, PHONE) VALUES (47,'Felipe','Izquierdo','Ave. 5 de Mayo Porlamar','I. de Margarita','4980','Venezuela','(8) 34-56-12');
insert into "T_USERS" (USER_ID, FIRST_NAME, LAST_NAME, ADDRESS, CITY, POSTAL_CODE, COUNTRY, PHONE) VALUES (48,'Fran','Wilson','89 Chiaroscuro Rd.','Portland','97219','USA','(503) 555-9573');
insert into "T_USERS" (USER_ID, FIRST_NAME, LAST_NAME, ADDRESS, CITY, POSTAL_CODE, COUNTRY, PHONE) VALUES (49,'Giovanni','Rovelli','Via Ludovico il Moro 22','Bergamo','24100','Italy','035-640230');
insert into "T_USERS" (USER_ID, FIRST_NAME, LAST_NAME, ADDRESS, CITY, POSTAL_CODE, COUNTRY, PHONE) VALUES (50,'Catherine','Dewey','Rue Joseph-Bens 532','Bruxelles','B-1180','Belgium','(02) 201 24 67');
insert into "T_USERS" (USER_ID, FIRST_NAME, LAST_NAME, ADDRESS, CITY, POSTAL_CODE, COUNTRY, PHONE) VALUES (51,'Jean','Fresniare','43 rue St. Laurent','Montréal','H1J 1C3','Canada','(514) 555-8054');
insert into "T_USERS" (USER_ID, FIRST_NAME, LAST_NAME, ADDRESS, CITY, POSTAL_CODE, COUNTRY, PHONE) VALUES (52,'Alexander','Feuer','Heerstr. 22','Leipzig','04179','Germany','0342-023176');
insert into "T_USERS" (USER_ID, FIRST_NAME, LAST_NAME, ADDRESS, CITY, POSTAL_CODE, COUNTRY, PHONE) VALUES (53,'Simon','Crowther','South House 300 Queensbridge','London','SW7 1RZ','UK','(171) 555-7733');
insert into "T_USERS" (USER_ID, FIRST_NAME, LAST_NAME, ADDRESS, CITY, POSTAL_CODE, COUNTRY, PHONE) VALUES (54,'Yvonne','Moncada','Ing. Gustavo Moncada 8585 Piso 20-A','Buenos Aires','1010','Argentina','(1) 135-5333');
insert into "T_USERS" (USER_ID, FIRST_NAME, LAST_NAME, ADDRESS, CITY, POSTAL_CODE, COUNTRY, PHONE) VALUES (55,'Rene','Phillips','2743 Bering St.','Anchorage','99508','USA','(907) 555-7584');
insert into "T_USERS" (USER_ID, FIRST_NAME, LAST_NAME, ADDRESS, CITY, POSTAL_CODE, COUNTRY, PHONE) VALUES (56,'Henriette','Pfalzheim','Mehrheimerstr. 369','Köln','50739','Germany','0221-0644327');
insert into "T_USERS" (USER_ID, FIRST_NAME, LAST_NAME, ADDRESS, CITY, POSTAL_CODE, COUNTRY, PHONE) VALUES (57,'Marie','Bertrand','265, boulevard Charonne','Paris','75012','France','(1) 42.34.22.66');
insert into "T_USERS" (USER_ID, FIRST_NAME, LAST_NAME, ADDRESS, CITY, POSTAL_CODE, COUNTRY, PHONE) VALUES (58,'Guillermo','Fernçndez','Calle Dr. Jorge Cash 321','México D.F.','05033','Mexico','(5) 552-3745');
insert into "T_USERS" (USER_ID, FIRST_NAME, LAST_NAME, ADDRESS, CITY, POSTAL_CODE, COUNTRY, PHONE) VALUES (59,'Georg','Pipps','Geislweg 14','Salzburg','5020','Austria','6562-9722');
insert into "T_USERS" (USER_ID, FIRST_NAME, LAST_NAME, ADDRESS, CITY, POSTAL_CODE, COUNTRY, PHONE) VALUES (60,'Isabel','de Castro','Estrada da sa£de n. 58','Lisboa','1756','Portugal','(1) 356-5634');
insert into "T_USERS" (USER_ID, FIRST_NAME, LAST_NAME, ADDRESS, CITY, POSTAL_CODE, COUNTRY, PHONE) VALUES (61,'Bernardo','Batista','Rua da Panificadora, 12','Rio de Janeiro','02389-673','Brazil','(21) 555-4252');
insert into "T_USERS" (USER_ID, FIRST_NAME, LAST_NAME, ADDRESS, CITY, POSTAL_CODE, COUNTRY, PHONE) VALUES (62,'Lecia','Carvalho','Alameda dos Canêrios, 891','Sao Paulo','05487-020','Brazil','(11) 555-1189');
insert into "T_USERS" (USER_ID, FIRST_NAME, LAST_NAME, ADDRESS, CITY, POSTAL_CODE, COUNTRY, PHONE) VALUES (63,'Horst','Kloss','Taucherstraße 10','Cunewalde','01307','Germany','0372-035188');
insert into "T_USERS" (USER_ID, FIRST_NAME, LAST_NAME, ADDRESS, CITY, POSTAL_CODE, COUNTRY, PHONE) VALUES (64,'Sergio','Gutiérrez','Av. del Libertador 900','Buenos Aires','1010','Argentina','(1) 123-5555');
insert into "T_USERS" (USER_ID, FIRST_NAME, LAST_NAME, ADDRESS, CITY, POSTAL_CODE, COUNTRY, PHONE) VALUES (65,'Paula','Wilson','2817 Milton Dr.','Albuquerque','87110','USA','(505) 555-5939');
insert into "T_USERS" (USER_ID, FIRST_NAME, LAST_NAME, ADDRESS, CITY, POSTAL_CODE, COUNTRY, PHONE) VALUES (66,'Maurizio','Moroni','Strada Provinciale 124','Reggio Emilia','42100','Italy','0522-556721');
insert into "T_USERS" (USER_ID, FIRST_NAME, LAST_NAME, ADDRESS, CITY, POSTAL_CODE, COUNTRY, PHONE) VALUES (67,'Janete','Limeira','Av. Copacabana, 267','Rio de Janeiro','02389-890','Brazil','(21) 555-3412');
insert into "T_USERS" (USER_ID, FIRST_NAME, LAST_NAME, ADDRESS, CITY, POSTAL_CODE, COUNTRY, PHONE) VALUES (68,'Michael','Holz','Grenzacherweg 237','Geneva','1203','Switzerland','0897-034214');
insert into "T_USERS" (USER_ID, FIRST_NAME, LAST_NAME, ADDRESS, CITY, POSTAL_CODE, COUNTRY, PHONE) VALUES (69,'Alejandra','Camino','Gran VÆa, 1','Madrid','28001','Spain','(91) 745 6200');
insert into "T_USERS" (USER_ID, FIRST_NAME, LAST_NAME, ADDRESS, CITY, POSTAL_CODE, COUNTRY, PHONE) VALUES (70,'Jonas','Bergulfsen','Erling Skakkes gate 78','Stavern','4110','Norway','07-98 92 35');
insert into "T_USERS" (USER_ID, FIRST_NAME, LAST_NAME, ADDRESS, CITY, POSTAL_CODE, COUNTRY, PHONE) VALUES (71,'Jose','Pavarotti','187 Suffolk Ln.','Boise','83720','USA','(208) 555-8097');
insert into "T_USERS" (USER_ID, FIRST_NAME, LAST_NAME, ADDRESS, CITY, POSTAL_CODE, COUNTRY, PHONE) VALUES (72,'Hari','Kumar','90 Wadhurst Rd.','London','OX15 4NB','UK','(171) 555-1717');
insert into "T_USERS" (USER_ID, FIRST_NAME, LAST_NAME, ADDRESS, CITY, POSTAL_CODE, COUNTRY, PHONE) VALUES (73,'Jytte','Petersen','Vinb¥ltet 34','Kobenhavn','1734','Denmark','31 12 34 56');
insert into "T_USERS" (USER_ID, FIRST_NAME, LAST_NAME, ADDRESS, CITY, POSTAL_CODE, COUNTRY, PHONE) VALUES (74,'Dominique','Perrier','25, rue Lauriston','Paris','75016','France','(1) 47.55.60.10');
insert into "T_USERS" (USER_ID, FIRST_NAME, LAST_NAME, ADDRESS, CITY, POSTAL_CODE, COUNTRY, PHONE) VALUES (75,'Art','Braunschweiger','P.O. Box 555','Lander','82520','USA','(307) 555-4680');
insert into "T_USERS" (USER_ID, FIRST_NAME, LAST_NAME, ADDRESS, CITY, POSTAL_CODE, COUNTRY, PHONE) VALUES (76,'Pascale','Cartrain','Boulevard Tirou, 255','Charleroi','B-6000','Belgium','(071) 23 67 22 20');
insert into "T_USERS" (USER_ID, FIRST_NAME, LAST_NAME, ADDRESS, CITY, POSTAL_CODE, COUNTRY, PHONE) VALUES (77,'Liz','Nixon','89 Jefferson Way Suite 2','Portland','97201','USA','(503) 555-3612');
insert into "T_USERS" (USER_ID, FIRST_NAME, LAST_NAME, ADDRESS, CITY, POSTAL_CODE, COUNTRY, PHONE) VALUES (78,'Liu','Wong','55 Grizzly Peak Rd.','Butte','59801','USA','(406) 555-5834');
insert into "T_USERS" (USER_ID, FIRST_NAME, LAST_NAME, ADDRESS, CITY, POSTAL_CODE, COUNTRY, PHONE) VALUES (79,'Karin','Josephs','Luisenstr. 48','Münster','44087','Germany','0251-031259');
insert into "T_USERS" (USER_ID, FIRST_NAME, LAST_NAME, ADDRESS, CITY, POSTAL_CODE, COUNTRY, PHONE) VALUES (80,'Miguel','Angel Paolino','Avda. Azteca 123','México D.F.','05033','Mexico','(5) 555-2933');
insert into "T_USERS" (USER_ID, FIRST_NAME, LAST_NAME, ADDRESS, CITY, POSTAL_CODE, COUNTRY, PHONE) VALUES (81,'Anabela','Domingues','Av. Inés de Castro, 414','Sao Paulo','05634-030','Brazil','(11) 555-2167');
insert into "T_USERS" (USER_ID, FIRST_NAME, LAST_NAME, ADDRESS, CITY, POSTAL_CODE, COUNTRY, PHONE) VALUES (82,'Helvetius','Nagy','722 DaVinci Blvd.','Kirkland','98034','USA','(206) 555-8257');
insert into "T_USERS" (USER_ID, FIRST_NAME, LAST_NAME, ADDRESS, CITY, POSTAL_CODE, COUNTRY, PHONE) VALUES (83,'Palle','Ibsen','Smagsloget 45','örhus','8200','Denmark','86 21 32 43');
insert into "T_USERS" (USER_ID, FIRST_NAME, LAST_NAME, ADDRESS, CITY, POSTAL_CODE, COUNTRY, PHONE) VALUES (84,'Mary','Saveley','2, rue du Commerce','Lyon','69004','France','78.32.54.86');
insert into "T_USERS" (USER_ID, FIRST_NAME, LAST_NAME, ADDRESS, CITY, POSTAL_CODE, COUNTRY, PHONE) VALUES (85,'Paul','Henriot','59 rue de l''Abbaye','Reims','51100','France','26.47.15.10');
insert into "T_USERS" (USER_ID, FIRST_NAME, LAST_NAME, ADDRESS, CITY, POSTAL_CODE, COUNTRY, PHONE) VALUES (86,'Rita','Müller','Adenauerallee 900','Stuttgart','70563','Germany','0711-020361');
insert into "T_USERS" (USER_ID, FIRST_NAME, LAST_NAME, ADDRESS, CITY, POSTAL_CODE, COUNTRY, PHONE) VALUES (87,'Pirkko','Koskitalo','Torikatu 38','Oulu','90110','Finland','981-443655');
insert into "T_USERS" (USER_ID, FIRST_NAME, LAST_NAME, ADDRESS, CITY, POSTAL_CODE, COUNTRY, PHONE) VALUES (88,'Paula','Parente','Rua do Mercado, 12','Resende','08737-363','Brazil','(14) 555-8122');
insert into "T_USERS" (USER_ID, FIRST_NAME, LAST_NAME, ADDRESS, CITY, POSTAL_CODE, COUNTRY, PHONE) VALUES (89,'Karl','Jablonski','305 - 14th Ave. S. Suite 3B','Seattle','98128','USA','(206) 555-4112');
insert into "T_USERS" (USER_ID, FIRST_NAME, LAST_NAME, ADDRESS, CITY, POSTAL_CODE, COUNTRY, PHONE) VALUES (90,'Matti','Karttunen','Keskuskatu 45','Helsinki','21240','Finland','90-224 8858');
insert into "T_USERS" (USER_ID, FIRST_NAME, LAST_NAME, ADDRESS, CITY, POSTAL_CODE, COUNTRY, PHONE) VALUES (91,'Zbyszek','Piestrzeniewicz','ul. Filtrowa 68','Warszawa','01-012','Poland','(26) 642-7012');