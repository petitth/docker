var dbname = "devdb";
var dbuser = "devuser";
var dbpassword = "devuser";

if (db.isMaster().ismaster) {
    var userExists = db.system.users.find({ user: dbuser }).count() == 1
    db = db.getSiblingDB(dbname);

    if (!userExists) {
        db.createUser(
            {
                user: dbuser,
                pwd: dbpassword,
                roles: [
                    { role: "readWrite", db: dbname },
                    { role: "clusterMonitor", db: "admin" }
                ]
            }
        );
        print("the user 'devuser' has been created");
    }

} else {
    print('current node is not master');
}
