ALTER SESSION SET CONTAINER = ORCLPDB1;

-- create user
create user oracle21 identified by oracle21;
-- add permissions
GRANT ALL PRIVILEGES TO oracle21;
