# Docker Tutorial

## Introduction

Docker is a set of platform as a service (PaaS) products that uses OS-level virtualization to deliver software in packages called containers.  
Containers are isolated from one another and bundle their own software, libraries and configuration files; they can communicate with each other through well-defined channels. All containers are run by a single operating system kernel and therefore use fewer resources than virtual machines.  
Think of a Docker container as a ready to used virtual machine, that you can create/remove with a command line.

link : www.docker.com  

Now install `Docker` on your machine.  
In the past, you would have installed a second tool called `Docker compose`, which is now part of Docker.

## Example 1 : First try

```bash
# try Docker with most basic container, which just displays a message and stops
docker run hello-world
# same, but giving a forced name to the container
docker run --name hello-container hello-world

# list the containers and the containers images
docker ps -a
docker image ls
```

You can see that you have 2 containers, stopped, using the same image.
Now that the test is over, let's clean them.

```bash
# first container has a generated name, visible with docker ps command
docker rm youthful_shannon
# second container has a defined name
docker rm hello-container
# now let's clean the image (same command works with container image id too)
docker image rm hello-world
```

## Example 2 : a linux container

Now let's move on with a container that you can actually use, for example, to get familiar with linux filesystem and linux commands.  
These 2 following containers are pretty similar, just pick one and try it.  
The command will create a linux debian container, with a defined name, and connect to it via the terminal, as root.  
The problem is that the container will stop if you leave it, typing `exit`.

```bash
docker run -it --name ubuntu-bash ubuntu /bin/bash
docker run -it --name debian-bash debian /bin/bash
```

Let's improve a bit : this time, the `run` command with launch a never ending process, and use detached mode.  
After that, we will connect to the container, in the terminal.  
If you leave the container, with `exit` command, you'll see that's it's not stopped (`docker ps -a`).

```bash
docker rm debian-bash # just to clean since we will create it again
docker run -d --name debian-bash debian tail -f /dev/null
docker exec -it debian-bash /bin/bash
```

Let's manipulate basic docker commands, for learning purposes.

```bash
docker stop debian-bash
docker start debian-bash
docker ps -a
# when it's up, you can connect to it
docker exec -it debian-bash /bin/bash
```

## Example 3 : build our own image

Start creating a file `Dockerfile-debian` with any text editor :

```bash
FROM debian:latest

# install text editors
RUN apt-get update
RUN apt-get install -y vim nano

# edit root profile (allow ll)
RUN echo "alias ll='ls $LS_OPTIONS -l'" >> /root/.bashrc

# add user
RUN useradd -s `which bash` -m guest
RUN echo guest:guest | chpasswd
# edit user profile (allow ll)
RUN echo "alias ll='ls $LS_OPTIONS -l'" >> /home/guest/.bashrc

# launch never ending process so that container keeps running
# (everytime the container starts)
ENTRYPOINT tail -f /dev/null
```

Create the docker image :

```bash
docker build -t my-debian . -f Dockerfile-debian
```

Confirm the new image exist : `docker image ls`

Now let's create a new container using the custom image.


```bash
# in case one already exists
docker stop debian-bash
docker rm debian-bash
# create it in detached mode
docker run -d --name debian-bash my-debian
# confirm the container exists and is running
docker ps -a
# connect to the container : try vi and nano for example
docker exec -it debian-bash /bin/bash
# switch to newly created user account
su guest
# disconnect as user
exit
# disconnect as root
exit
# confirm it's still running
docker ps -a
```

Now you know how to create your own docker image.  
With this example, you can get familiar with linux : the filesystem, the commands, and basic text editors (`vi` and `nano`).  
remark : `vi` editor is not funniest editor but it's usually the one you're sure to find on any linux system  
See annex C with `vi` fundamentals.

## Example 4 : mapping the container port

Let's discover another important concept : mapping your container port.  
For this, we'll use a spring boot application with minimal code, and another custom container image.  
By default, a springboot application is using port `8080`.    
We will map the container port to a different port on the host machine.

Try this :
* create a folder called `spring-boot-app`
* create `spring-boot-app/pom.xml`

```xml
<project xmlns = "http://maven.apache.org/POM/4.0.0"
         xmlns:xsi = "http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation = "http://maven.apache.org/POM/4.0.0
   http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>edu</groupId>
    <artifactId>spring-boot-rest</artifactId>
    <version>1.0.0</version>
    <packaging>jar</packaging>

    <parent>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-parent</artifactId>
        <version>2.6.3</version>
    </parent>

    <dependencies>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>
    </dependencies>

    <build>
        <finalName>application</finalName>
        <plugins>
            <plugin>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin</artifactId>
            </plugin>
        </plugins>
    </build>
</project>
```

* create `spring-boot-app/src/main/java/edu/SpringBootApp.java`

```java
package edu;

import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@SpringBootApplication
@RestController
public class SpringBootApp {

    public static void main(String[] args) {
        SpringApplication springApplication = new SpringApplication(SpringBootApp.class);
        springApplication.setBannerMode(Banner.Mode.OFF);
        springApplication.run(args);
    }

    @GetMapping("/")
    public @ResponseBody Map<String, Object> getTimeWithRest() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Map<String, Object> map = new HashMap<>();
        map.put("time", sdf.format(new Date()) );
        map.put("timezone", System.getenv("TZ") != null ? System.getenv("TZ") : "unknown");        
        return map;
    }
}
```

remark : a better approach would be to declare the web service in a separated class, and also declare an `application.properties` or `application.yml`, but we'll keep it simple for learning purposes

* create the following custom image : `Dockerfile-springboot`

```bash
FROM maven:3.8.1-jdk-11

COPY spring-boot-app/src ./src
COPY spring-boot-app/pom.xml ./

# build project inside the container
RUN mvn clean package
# used command on docker run
ENTRYPOINT java -jar target/application.jar
```

* build the docker image

```bash
docker build -t my-springboot . -f Dockerfile-springboot
```

* create a container, mapping default container port `8080` to local port `8088`  
This way you can create multiple containers, pointing to a different local port.

```bash
docker run -p 8088:8080 -d --name springboot8088 my-springboot
```

* try it : open you browser and call `http://localhost:8088/`  
You just called the rest service returning current time.

## Example 5 : Docker Compose

Docker compose lets you declare one or multiple containers in a yaml file, and create them all at once, with one command.   
It's truly helpful, as all the parameters will be defined in the yaml.

Lets start by removing every existing container and container image, then move on with next example.  
```bash
# stop and clear all containers (+ associated volumes)
docker rm -v -f $(docker ps -qa)
# remove all container images
docker image remove -f $(docker images -a -q)
```


Then create `docker-compose-training.yaml` :

```yaml
services:
  hello:
    image: "hello-world"
    container_name: hello-training
  linux-debian:
    container_name: debian-bash
    image: linux-debian-custom-img
    build:
      context: .
      dockerfile: Dockerfile-debian
    environment:
      - FOO=bar

  springboot-rest-a:
    container_name: springboot-app-a
    image: springboot-rest-img
    ports:
      - 8085:8080
    build:
      context: .
      dockerfile: Dockerfile-springboot
    environment:
      - TZ=America/New_York

  springboot-rest-b:
    container_name: springboot-app-b
    image: springboot-rest-img
    ports:
      - 8086:8080
    build:
      context: .
      dockerfile: Dockerfile-springboot
    environment:
      - TZ=America/Los_Angeles

  springboot-rest-c:
    container_name: springboot-app-c
    image: maven:3.8.1-jdk-11
    ports:
      - 8087:8080
    working_dir: /spring-boot-app
    volumes:
      - ./spring-boot-app:/spring-boot-app
    command: mvn clean spring-boot:run
    environment:
      - TZ=Europe/Brussels
```

Launch the command to build the containers (in detached mode) :

```bash
docker compose -f ./docker-compose-training.yaml up -d
```

Check the containers' status :

```bash
docker ps -a
```

You should see 5 containers :
1) `hello-training` container just shows the basics, it's the same test as first example
2) `debian-bash` shows how to create the custom image from example 3, but with `Docker compose`
3) `springboot-app-a` and `springboot-app-b` are using the same custom image, but are mapped to a different port
4) `springboot-app-c` is a maven container where the springboot application is injected, via a volume, and launched

Please notice another instruction which was not used in the previous examples : `environment` section is used to create environment variables in the containers.  
Lets check them :

```bash
docker exec -it debian-bash /bin/bash
echo $FOO
exit

docker exec -it springboot-app-a /bin/bash
echo $TZ
exit
```

The `$TZ` parameter sets the timezone in a linux container, the springboot containers being linux containers with maven and jdk 11.  

Now, open a web browser and call the web service from the spring boot containers.  
This is a basic get call, returning current time based on a certain timezone.

```text
# call rest web service of springboot-app-a
http://localhost:8085
# call rest web service of springboot-app-b
http://localhost:8086
# call rest web service of springboot-app-c
http://localhost:8087
```

---

## Annex A : Practical commands

```bash
# -- basics --
- connect to a named container : docker exec -it <container-name> /bin/bash
- retrieve a file from container to host machine : docker cp <container-name>:/tmp/something.jar /tmp
- start a container : docker start <container-name>
- stop a container : docker stop <container-name>
- remove a container and associated volumes : docker rm <container-name> --volumes

# -- Images / Volumes / Network --
- existing images : docker image ls
- remove an image using id : docker image rm <image-id>
- force removing an image using id : docker image rm <image-id> --force
- check created networks : docker network ls
- remove one network : docker network rm <name>
- check existing volumes : docker volume ls
- remove one volume : docker volume rm <volume-id>
- clear all non used volumes : docker volume prune --force

# -- Logs --
- check container log : docker logs <container-name>
- get last 50 lines of log : docker logs -f --tail 50 <container-name>
```


## Annex B : Links

See official containers : https://registry.hub.docker.com/search?q=&type=image


## Annex C : Vi basics

VI is a text editor, present on almost all Linux distributions. <br/>
You can use it in a terminal to create/edit a file with ```vi filename```

By default, you'll be on command mode. Press `i` to switch to insert mode (see "INSERT" at the bottom of the screen).    
Press `ESC` to switch back to command mode.

Here are some basic commands :

```text
# basics
:wq!                    save and quit          
:q!                     quit without saving    
:w                      save                   
u                       cancel last command

# misc    
:set number             add line numbers
:set nonumber           remove line numbers
```
